function getSolarSystem()
{
    let mySolarSystem = new SolarSystem();

    let oTest1 = new OrbitalObject('Test');
    let oTest2 = new OrbitalObject('Test 2');

    oTest2.sma = 20;

    mySolarSystem.orbitalObjects.push(oTest1);
    mySolarSystem.orbitalObjects[0].addChild(oTest2);
    console.log(mySolarSystem);
    return mySolarSystem;
}

class SolarSystem
{
    status = -1;
    orbitalObjects = [];

    constructor()
    {
        this.status = 0;
    }
}

class OrbitalObject
{
    name = "";

    parentObject = undefined;
    childObjects = [];

    sma = 75;
    ecc = 0.3;
    inc = radians(30);
    argPeri = 0;
    lan = radians(30);
    meanAnom = 0.1;
    epoch = 0;

    constructor(_name)
    {
        this.name = _name;
    }

    addChild (toAdd)
    {
        toAdd.parentObject = this;
        this.childObjects.push(toAdd);
    }
}


function drawSolarSystem(SS,time)
{
    for (const myObj of SS.orbitalObjects)
    {
        drawOrbitalObject(myObj, time);
    }

}

function drawOrbitalObject(orbObj, time)
{
    drawOrbit(orbObj);
    let offsets = drawOrbitPoint(orbObj, time);
    push();
    translate(offsets.xOffset, offsets.yOffset, offsets.zOffset);
    for (const myObj of orbObj.childObjects)
    {
        drawOrbitalObject(myObj, time);
    }
    pop();
}

function drawOrbitPoint(orb, T)
{
    if ( abs(orb.epoch-T) > 1e-6) // Tune this value
    {
        //Assume period of 10s for a orbit of sma = 75 for now
        let period = 0.015 * sqrt( orb.sma ** 3 );
        let timeDelta = T-orb.epoch;
        orb.meanAnom = (orb.meanAnom + (timeDelta/period * TWO_PI)) % TWO_PI; // modulo TWO_PI to keep in range of 0 to 2*pi
        orb.epoch = T;
    }
    trueAnom = solveTrueAnom(orb.meanAnom, orb.ecc);

    // Orbital plane coords
    x_orb = orb.sma * (cos(trueAnom) - orb.ecc);
    y_orb = orb.sma * sqrt(1-orb.ecc**2) * sin(trueAnom);
    z_orb = 0;

    // Transform to global frame
    let ot = getOrbitTransform(orb);
    x = ot.xx * x_orb + ot.xy * y_orb;
    y = ot.yx * x_orb + ot.yy * y_orb;
    z = ot.zx * x_orb + ot.zy * y_orb;

    //console.log(x,y,z);
    push();
    strokeWeight(20);
    stroke(5);
    point(x,y,z);
    pop();

    return {xOffset: x, yOffset: y, zOffset: z};
}

function drawOrbit(orb)
{
    // Get orbital plane transformation
    let ot = getOrbitTransform(orb);

    let xArray = [];
    let yArray = [];
    let zArray = [];

    for(let trueAnom = 0; trueAnom<=TWO_PI; trueAnom+=0.1)
    {
        // Orbital plane coords
        x_orb = orb.sma * (cos(trueAnom) - orb.ecc);
        y_orb = orb.sma * sqrt(1-orb.ecc**2) * sin(trueAnom);
        z_orb = 0;

        // Do transform
        xArray.push(ot.xx * x_orb + ot.xy * y_orb);
        yArray.push(ot.yx * x_orb + ot.yy * y_orb);
        zArray.push(ot.zx * x_orb + ot.zy * y_orb);
    }

    push();
    strokeWeight(1);
    stroke('#fcba03');
    for(let i = 1; i<xArray.length; i++)
    {
        line(xArray[i-1], yArray[i-1], zArray[i-1], xArray[i], yArray[i], zArray[i]);
    }
    // Close loop
    line(xArray[xArray.length-1], yArray[xArray.length-1], zArray[xArray.length-1], xArray[0], yArray[0], zArray[0]);
    pop();
}

function getOrbitTransform(orb)
{
    _xx = (cos(orb.argPeri)*cos(orb.lan) - sin(orb.argPeri)*sin(orb.lan)*cos(orb.inc));
    _xy = (-sin(orb.argPeri)*cos(orb.lan) - cos(orb.argPeri)*sin(orb.lan)*cos(orb.inc));
    _yx = (cos(orb.argPeri)*sin(orb.lan) + sin(orb.argPeri)*cos(orb.lan)*cos(orb.inc));
    _yy = (-sin(orb.argPeri)*sin(orb.lan) + cos(orb.argPeri)*cos(orb.lan)*cos(orb.inc));
    _zx = (sin(orb.argPeri)*sin(orb.inc));
    _zy = (cos(orb.argPeri)*sin(orb.inc));
    myOrbTransform =
        {
            xx:_xx,
            xy:_xy,
            yx:_yx,
            yy:_yy,
            zx:_zx,
            zy:_zy };
    return myOrbTransform;
}

function solveTrueAnom(M, e)
{
    let nu = M - e * sin(M); // First guess of true anomaly (nu)
    let deltaNu = Infinity;
    let deltaM = Infinity;

    while (deltaNu > 1e-5)
    {
        deltaM = M - (nu - e*sin(nu));
        deltaNu = deltaM / (1-e*cos(nu));
        nu += deltaNu;
    }
    return nu;
}