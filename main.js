var testSolarSystem;
let incSlider;
let lanSlider;
let myFont;
let time = 0;

function preload()
{
    console.log('Done!');
}

function setup()
{
    createCanvas(600, 600, WEBGL);
    //Inc
    let incDiv = createDiv();
    let incLabel = createP('Inclination:');
    incSlider = createSlider(0, 180, 30);
    incSlider.style('width', '500px');
    incLabel.parent(incDiv);
    incSlider.parent(incDiv);
    //Lan
    let lanDiv = createDiv();
    let lanLabel = createP('Longitude of the Ascending Node:');
    lanSlider = createSlider(0, 360, 30);
    lanSlider.style('width', '500px');
    lanLabel.parent(lanDiv);
    lanSlider.parent(lanDiv);

    testSolarSystem = getSolarSystem();
}



function draw()
{
    scale(1, 1, -1); // Switch to the only proper coordinate system (right handed)
    time = time + deltaTime/1000;
    background(100);
    push();
    stroke(0);
    strokeWeight(0.5);
    fill('#5e6df2');
    rotateX(HALF_PI);
    sphere(20);
    pop();
    testSolarSystem.orbitalObjects[0].inc = radians( incSlider.value() );
    testSolarSystem.orbitalObjects[0].lan = radians( lanSlider.value() );


    
    drawSolarSystem(testSolarSystem, time);

    camera(0, 200, 0, 0, 0, 0, 0, 0, 1)
}

function limit(x, minX, maxX)
{
    return(max(min(x, maxX), minX));
}